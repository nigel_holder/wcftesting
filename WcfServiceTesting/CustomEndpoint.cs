﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.Web;

namespace WcfServiceTesting
{
    public class CustomEndpoint:ServiceEndpoint
    {
        public CustomEndpoint()
            :this(string.Empty)
        {

        }
        public CustomEndpoint(string address)
            :this(address, ContractDescription.GetContract(typeof(ISecondService)))
        {

        }
        public CustomEndpoint(string address, ContractDescription contract):base(contract)
        {
            this.Binding = new BasicHttpBinding();
            this.IsSystemEndpoint = false; 
        }

        //This is an additional custom property
        public int RetryCount { get; set; }
    }

    public class CustomEndpointElement : StandardEndpointElement
    {
        public int RetryCount { get { return (int)base["retrycount"]; } set { base["retryCount"] = value; } }

        protected override Type EndpointType
        {
            get
            {
                return typeof(CustomEndpoint);
            }
        }

        protected override ConfigurationPropertyCollection Properties
        {
            get
            {
                ConfigurationPropertyCollection properties = base.Properties;
                properties.Add(new ConfigurationProperty("retrycount", typeof(int), 5, ConfigurationPropertyOptions.None));
                return properties;
            }
        }

        protected override ServiceEndpoint CreateServiceEndpoint(ContractDescription contractDescription)
        {
            return new CustomEndpoint();
        }

        protected override void OnApplyConfiguration(ServiceEndpoint endpoint, ServiceEndpointElement serviceEndpointElement)
        {
            CustomEndpoint customEndpoint = (CustomEndpoint)endpoint;
            customEndpoint.RetryCount = this.RetryCount;
           
        }

        protected override void OnApplyConfiguration(ServiceEndpoint endpoint, ChannelEndpointElement channelEndpointElement)
        {
            CustomEndpoint customEndpoint = (CustomEndpoint)endpoint;
            customEndpoint.RetryCount = this.RetryCount;

        }

        protected override void OnInitializeAndValidate(ServiceEndpointElement serviceEndpointElement)
        {
           
        }

        protected override void OnInitializeAndValidate(ChannelEndpointElement channelEndpointElement)
        {
          
        }
    }
    public class CustomEndpointCollectionElement : StandardEndpointCollectionElement<CustomEndpoint, CustomEndpointElement> { }
}