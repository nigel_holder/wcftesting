﻿using DataAccessLayer.Models;
using DataAccessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;

namespace WcfServiceTesting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SecondService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SecondService.svc or SecondService.svc.cs at the Solution Explorer and start debugging.
    public class SecondService : ISecondService
    {
        private readonly IBaseRepository<Activity> _events;
        private readonly IBaseRepository<Location> _locations;
        private readonly IBaseRepository<People> _people;

        public SecondService(IBaseRepository<Activity> events, IBaseRepository<People> people, IBaseRepository<Location> locations)
        {
            _events = events;
            _people = people;
            _locations = locations;
        }
        public SecondService()
        {
            var context = new DataAccessLayer.Contexts.DataAccessContext();

            _people = new BaseRepository<People>(context);
            _events = new BaseRepository<Activity>(context);
            _locations = new BaseRepository<Location>(context);
        }
        public Activity GetEvent(int id)
        {
            var eve = _events.Get().Where(x => x.Id == id).FirstOrDefault();
            return eve;
        }

        public IEnumerable<Activity> GetEventsByLocation(int locationId)
        {
            return _events.SearchActive(x => x.LocationId == locationId);
        }

        public IEnumerable<Location> GetLocations()
        {
            return _locations.Get()
                  .Skip(100).Take(15);
        }
        public IEnumerable<People> GetUsers()
        {

            return _people.Get().Take(15);
        }
        public People GetUser(int id)
        { return _people.GetByID(id); }
    }
}
