﻿using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceTesting
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISecondService" in both code and config file together.
    [ServiceContract]
    public interface ISecondService
    {
        [OperationContract]
        IEnumerable<People> GetUsers();

        [OperationContract]
        Activity GetEvent(int id);

        [OperationContract]

        IEnumerable<Activity> GetEventsByLocation(int locationId);

        [OperationContract]
        IEnumerable<Location> GetLocations();

        [OperationContract]
        People GetUser(int id);
    }
}
