namespace DataAccessLayer.Migrations
{
    using Contexts;
    using Models;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataAccessContext>
    {
        public Configuration()
        {

            AutomaticMigrationsEnabled = false;
        }
        public void SeedDebug(DataAccessContext context)
        {
            Seed(context);
        }
        protected override void Seed(DataAccessContext context)
        {
          
            var eventsJson = JsonConvert.DeserializeObject<List<Activity>>(File.ReadAllText(@"C:\Users\nigel.holder\Desktop\Code\WCFTest\WcfServiceTesting\DataAccessLayer\Json\Events.json"));

            var peopleJson = JsonConvert.DeserializeObject<List<People>>(File.ReadAllText(@"C:\Users\nigel.holder\Desktop\Code\WCFTest\WcfServiceTesting\DataAccessLayer\Json\People.json"));

            var locationJson = JsonConvert.DeserializeObject<List<Location>>(File.ReadAllText(@"C:\Users\nigel.holder\Desktop\Code\WCFTest\WcfServiceTesting\DataAccessLayer\Json\Locations.json"));

            foreach (var item in locationJson)
            {
                context.Locations.AddOrUpdate(x => x.Id, item);
            }
            foreach (var item in eventsJson)
            {
                context.Events.AddOrUpdate(x => x.Id, item);
            }
            foreach (var item in peopleJson)
            {
                context.People.AddOrUpdate(x => x.Id, item);
            }
        } }
    }

