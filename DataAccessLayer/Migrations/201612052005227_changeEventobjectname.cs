namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeEventobjectname : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Events", newName: "Activities");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Activities", newName: "Events");
        }
    }
}
