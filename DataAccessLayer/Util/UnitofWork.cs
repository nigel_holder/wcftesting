using System.Collections;

namespace DataAccessLayer
{
    public class UnitOfWork
    {
        //this needs to be the interface
        private readonly DataAccessLayer.Contexts.DataAccessContext _context;
        private Hashtable _repositories;
    }
    /// <summary>
        /// Gets the repository for the specified entity type.
        /// </summary>
        /// <typeparam name="T">The entity type.</typeparam>
        /// <returns>The repository for the specified entity type.</returns>
        public IBaseRepository<T> Repository<T>() where T : class
        {
            if (_repositories == null)
                _repositories = new Hashtable();

            string type = typeof(T).Name;

            if (!_repositories.ContainsKey(type))
            {
                Type repositoryType = typeof(BaseRepository<>);

                object repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), _context);

                _repositories.Add(type, repositoryInstance);
            }

            return (IBaseRepository<T>)_repositories[type];
        }

        /// <summary>
        /// Executes a defined SQL command.
        /// </summary>
        /// <param name="command">The database command to execute. This will derive from the base DatabaseCommand type.</param>
        /// <param name="parameters">The parameters for the command.</param>
        /// <returns>The result of the SQL command.</returns>
        public int ExecuteCommand(DatabaseCommand command, params IDataParameter[] parameters)
        {
            parameters = parameters.Where(p => p != null && !string.IsNullOrWhiteSpace(p.ParameterName)).ToArray();

            string sql =
                "exec "
                + command.ToString()
                + " "
                + string.Join(", ", parameters.Select(p => p.ParameterName + " = " + p.ParameterName));

            return _context.Database.ExecuteSqlCommand(sql, parameters);
        }

        /// <summary>
        /// Asynchronously executes a defined SQL command.
        /// </summary>
        /// <param name="command">The database command to execute. This will derive from the base DatabaseCommand type.</param>
        /// <param name="parameters">The parameters for the command.</param>
        /// <returns>The result of the SQL command.</returns>
        public async Task<int> ExecuteCommandAsync(DatabaseCommand command, params IDataParameter[] parameters)
        {
            parameters = parameters.Where(p => p != null && !string.IsNullOrWhiteSpace(p.ParameterName)).ToArray();

            string sql =
                "exec "
                + command.ToString()
                + " "
                + string.Join(", ", parameters.Select(p => p.ParameterName + " = " + p.ParameterName));

            int result = await _context.Database.ExecuteSqlCommandAsync(sql, parameters);

            return result;
        }

        /// <summary>
        /// Executes a defined SQL query.
        /// </summary>
        /// <typeparam name="T">The return type to map to.</typeparam>
        /// <param name="query">The database query to execute. This will derive from the base DatabaseQuery type.</param>
        /// <param name="parameters">The parameters for the query.</param>
        /// <returns>The SQL query result of type T.</returns>
        public IEnumerable<T> ExecuteQuery<T>(DatabaseQuery query, params IDataParameter[] parameters)
        {
            parameters = parameters.Where(p => p != null && !string.IsNullOrWhiteSpace(p.ParameterName)).ToArray();

            string sql =
                "exec "
                + query.ToString()
                + " "
                + string.Join(", ", parameters.Select(p => p.ParameterName + " = " + p.ParameterName));

            return _context.Database.SqlQuery<T>(sql, parameters);
        }

        /// <summary>
        /// Asynchronously executes a defined SQL query.
        /// </summary>
        /// <typeparam name="T">The return type to map to.</typeparam>
        /// <param name="query">The database query to execute. This will derive from the base DatabaseQuery type.</param>
        /// <param name="parameters">The parameters for the query.</param>
        /// <returns>The SQL query result of type T.</returns>
        public async Task<List<T>> ExecuteQueryAsync<T>(DatabaseQuery query, params IDataParameter[] parameters)
        {
            parameters = parameters.Where(p => p != null && !string.IsNullOrWhiteSpace(p.ParameterName)).ToArray();

            string sql =
                "exec "
                + query.ToString()
                + " "
                + string.Join(", ", parameters.Select(p => p.ParameterName + " = " + p.ParameterName));

            List<T> result = await _context.Database.SqlQuery<T>(sql, parameters).ToListAsync();

            return result;
        }

        /// <summary>
        /// Saves the entity changes to the database.
        /// </summary>
        public void Save()
        {
            _context.SaveChanges();
        }

        /// <summary>
        /// Asynchronously saves the entity changes to the database.
        /// </summary>
        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        #region IDisposable
        private bool _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    _context.Dispose();
                }
            }
            _isDisposed = true;
        }
        #endregion
    }
}