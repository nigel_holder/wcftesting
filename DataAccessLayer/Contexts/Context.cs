﻿using DataAccessLayer.Models;
using System;
using System.Data.Entity;

namespace DataAccessLayer.Contexts
{
    public class DataAccessContext : DbContext, IDisposable
    {
        public DataAccessContext() : base("DataAccessContext")
        {

        }

        public DbSet<Activity> Events { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<People> People { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>()
               .Configure(c => c.HasColumnType("datetime2"));
            base.OnModelCreating(modelBuilder);
        }


    }

}

