﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{

    public class Activity : BaseEntity
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Organizer { get; set; }
        [DataMember]
        public DateTime? EventDate { get; set; }
        [DataMember]
        public bool IsRecurring { get; set; }
        [DataMember]
        public int LocationId { get; set; }
        [DataMember]
        public virtual Location Location { get; set; }
    }
}
