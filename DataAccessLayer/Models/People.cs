﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public class People: BaseEntity
    {
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
