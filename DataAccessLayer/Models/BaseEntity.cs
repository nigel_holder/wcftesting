﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    [DataContract]
    public abstract class BaseEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
        [IgnoreDataMember]
        public bool IsDeleted { get; set; }
    }
}
