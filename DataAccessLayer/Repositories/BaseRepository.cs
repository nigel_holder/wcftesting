﻿using DataAccessLayer.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DataAccessLayer.Repositories
{


    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly DataAccessContext _context;
        public BaseRepository(DataAccessContext context)
        {
            context.Configuration.ProxyCreationEnabled = false;
            _context = context;
        }
        public Task Add(T entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> Get()
        {
            return _context.Set<T>()
                 .AsNoTracking()
                 .AsEnumerable<T>();
        }

        public IEnumerable<T> GetAllActive()
        {
            return _context.Set<T>()
                .AsNoTracking()
                .Where(x => !x.IsDeleted)
                .AsEnumerable<T>();
        }

        public T GetByID(int id)
        {
            return _context.Set<T>().Where(x => x.Id ==id).FirstOrDefault();
        }

        public Task Remove(int id)
        {
            var entity = _context.Set<T>().Find(id);
            _context.Set<T>().Remove(entity);
            return _context.SaveChangesAsync();
        }

        public IEnumerable<T> Search(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate).AsEnumerable<T>();
        }

        public IEnumerable<T> SearchActive(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(x => !x.IsDeleted)
                .Where(predicate).AsEnumerable<T>();
        }

        public async Task<IEnumerable<T>> SearchAsync(Expression<Func<T, bool>> predicate)
        {
            return await _context.Set<T>().Where(x => !x.IsDeleted)
             .Where(predicate).ToListAsync();
        }

        public Task Update(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
