﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        Task Add(T entity);

        IEnumerable<T> Get();

        IEnumerable<T> GetAllActive();

        T GetByID(int id);

        Task Remove(int id);

        IEnumerable<T> Search(Expression<Func<T, bool>> predicate);

        IEnumerable<T> SearchActive(Expression<Func<T, bool>> predicate);

        Task<IEnumerable<T>> SearchAsync(Expression<Func<T, bool>> predicate);

        Task Update(T entity);
    }
}
