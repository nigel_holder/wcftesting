﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WPFConsumer.DemoService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="People", Namespace="http://schemas.datacontract.org/2004/07/DataAccessLayer.Models")]
    [System.SerializableAttribute()]
    public partial class People : WPFConsumer.DemoService.BaseEntity {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime DateOfBirthField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DateOfBirth {
            get {
                return this.DateOfBirthField;
            }
            set {
                if ((this.DateOfBirthField.Equals(value) != true)) {
                    this.DateOfBirthField = value;
                    this.RaisePropertyChanged("DateOfBirth");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="BaseEntity", Namespace="http://schemas.datacontract.org/2004/07/DataAccessLayer")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(WPFConsumer.DemoService.Activity))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(WPFConsumer.DemoService.Location))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(WPFConsumer.DemoService.People))]
    public partial class BaseEntity : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime DateCreatedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IdField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DateCreated {
            get {
                return this.DateCreatedField;
            }
            set {
                if ((this.DateCreatedField.Equals(value) != true)) {
                    this.DateCreatedField = value;
                    this.RaisePropertyChanged("DateCreated");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Activity", Namespace="http://schemas.datacontract.org/2004/07/DataAccessLayer.Models")]
    [System.SerializableAttribute()]
    public partial class Activity : WPFConsumer.DemoService.BaseEntity {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.DateTime> EventDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsRecurringField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private WPFConsumer.DemoService.Location LocationField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int LocationIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string OrganizerField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> EventDate {
            get {
                return this.EventDateField;
            }
            set {
                if ((this.EventDateField.Equals(value) != true)) {
                    this.EventDateField = value;
                    this.RaisePropertyChanged("EventDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsRecurring {
            get {
                return this.IsRecurringField;
            }
            set {
                if ((this.IsRecurringField.Equals(value) != true)) {
                    this.IsRecurringField = value;
                    this.RaisePropertyChanged("IsRecurring");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public WPFConsumer.DemoService.Location Location {
            get {
                return this.LocationField;
            }
            set {
                if ((object.ReferenceEquals(this.LocationField, value) != true)) {
                    this.LocationField = value;
                    this.RaisePropertyChanged("Location");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int LocationId {
            get {
                return this.LocationIdField;
            }
            set {
                if ((this.LocationIdField.Equals(value) != true)) {
                    this.LocationIdField = value;
                    this.RaisePropertyChanged("LocationId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Organizer {
            get {
                return this.OrganizerField;
            }
            set {
                if ((object.ReferenceEquals(this.OrganizerField, value) != true)) {
                    this.OrganizerField = value;
                    this.RaisePropertyChanged("Organizer");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Location", Namespace="http://schemas.datacontract.org/2004/07/DataAccessLayer.Models")]
    [System.SerializableAttribute()]
    public partial class Location : WPFConsumer.DemoService.BaseEntity {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FriendlyNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<decimal> LatitudeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<decimal> LongitudeField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FriendlyName {
            get {
                return this.FriendlyNameField;
            }
            set {
                if ((object.ReferenceEquals(this.FriendlyNameField, value) != true)) {
                    this.FriendlyNameField = value;
                    this.RaisePropertyChanged("FriendlyName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<decimal> Latitude {
            get {
                return this.LatitudeField;
            }
            set {
                if ((this.LatitudeField.Equals(value) != true)) {
                    this.LatitudeField = value;
                    this.RaisePropertyChanged("Latitude");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<decimal> Longitude {
            get {
                return this.LongitudeField;
            }
            set {
                if ((this.LongitudeField.Equals(value) != true)) {
                    this.LongitudeField = value;
                    this.RaisePropertyChanged("Longitude");
                }
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="DemoService.ISecondService")]
    public interface ISecondService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetUsers", ReplyAction="http://tempuri.org/ISecondService/GetUsersResponse")]
        WPFConsumer.DemoService.GetUsersResponse GetUsers(WPFConsumer.DemoService.GetUsersRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetUsers", ReplyAction="http://tempuri.org/ISecondService/GetUsersResponse")]
        System.Threading.Tasks.Task<WPFConsumer.DemoService.GetUsersResponse> GetUsersAsync(WPFConsumer.DemoService.GetUsersRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetEvent", ReplyAction="http://tempuri.org/ISecondService/GetEventResponse")]
        WPFConsumer.DemoService.GetEventResponse GetEvent(WPFConsumer.DemoService.GetEventRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetEvent", ReplyAction="http://tempuri.org/ISecondService/GetEventResponse")]
        System.Threading.Tasks.Task<WPFConsumer.DemoService.GetEventResponse> GetEventAsync(WPFConsumer.DemoService.GetEventRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetEventsByLocation", ReplyAction="http://tempuri.org/ISecondService/GetEventsByLocationResponse")]
        WPFConsumer.DemoService.GetEventsByLocationResponse GetEventsByLocation(WPFConsumer.DemoService.GetEventsByLocationRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetEventsByLocation", ReplyAction="http://tempuri.org/ISecondService/GetEventsByLocationResponse")]
        System.Threading.Tasks.Task<WPFConsumer.DemoService.GetEventsByLocationResponse> GetEventsByLocationAsync(WPFConsumer.DemoService.GetEventsByLocationRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetLocations", ReplyAction="http://tempuri.org/ISecondService/GetLocationsResponse")]
        WPFConsumer.DemoService.GetLocationsResponse GetLocations(WPFConsumer.DemoService.GetLocationsRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetLocations", ReplyAction="http://tempuri.org/ISecondService/GetLocationsResponse")]
        System.Threading.Tasks.Task<WPFConsumer.DemoService.GetLocationsResponse> GetLocationsAsync(WPFConsumer.DemoService.GetLocationsRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetUser", ReplyAction="http://tempuri.org/ISecondService/GetUserResponse")]
        WPFConsumer.DemoService.GetUserResponse GetUser(WPFConsumer.DemoService.GetUserRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISecondService/GetUser", ReplyAction="http://tempuri.org/ISecondService/GetUserResponse")]
        System.Threading.Tasks.Task<WPFConsumer.DemoService.GetUserResponse> GetUserAsync(WPFConsumer.DemoService.GetUserRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetUsers", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetUsersRequest {
        
        public GetUsersRequest() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetUsersResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetUsersResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public System.Collections.ObjectModel.ObservableCollection<WPFConsumer.DemoService.People> GetUsersResult;
        
        public GetUsersResponse() {
        }
        
        public GetUsersResponse(System.Collections.ObjectModel.ObservableCollection<WPFConsumer.DemoService.People> GetUsersResult) {
            this.GetUsersResult = GetUsersResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetEvent", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetEventRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public int id;
        
        public GetEventRequest() {
        }
        
        public GetEventRequest(int id) {
            this.id = id;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetEventResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetEventResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public WPFConsumer.DemoService.Activity GetEventResult;
        
        public GetEventResponse() {
        }
        
        public GetEventResponse(WPFConsumer.DemoService.Activity GetEventResult) {
            this.GetEventResult = GetEventResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetEventsByLocation", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetEventsByLocationRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public int locationId;
        
        public GetEventsByLocationRequest() {
        }
        
        public GetEventsByLocationRequest(int locationId) {
            this.locationId = locationId;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetEventsByLocationResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetEventsByLocationResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public System.Collections.ObjectModel.ObservableCollection<WPFConsumer.DemoService.Activity> GetEventsByLocationResult;
        
        public GetEventsByLocationResponse() {
        }
        
        public GetEventsByLocationResponse(System.Collections.ObjectModel.ObservableCollection<WPFConsumer.DemoService.Activity> GetEventsByLocationResult) {
            this.GetEventsByLocationResult = GetEventsByLocationResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetLocations", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetLocationsRequest {
        
        public GetLocationsRequest() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetLocationsResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetLocationsResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public System.Collections.ObjectModel.ObservableCollection<WPFConsumer.DemoService.Location> GetLocationsResult;
        
        public GetLocationsResponse() {
        }
        
        public GetLocationsResponse(System.Collections.ObjectModel.ObservableCollection<WPFConsumer.DemoService.Location> GetLocationsResult) {
            this.GetLocationsResult = GetLocationsResult;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetUser", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetUserRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public int id;
        
        public GetUserRequest() {
        }
        
        public GetUserRequest(int id) {
            this.id = id;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetUserResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class GetUserResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public WPFConsumer.DemoService.People GetUserResult;
        
        public GetUserResponse() {
        }
        
        public GetUserResponse(WPFConsumer.DemoService.People GetUserResult) {
            this.GetUserResult = GetUserResult;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISecondServiceChannel : WPFConsumer.DemoService.ISecondService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SecondServiceClient : System.ServiceModel.ClientBase<WPFConsumer.DemoService.ISecondService>, WPFConsumer.DemoService.ISecondService {
        
        public SecondServiceClient() {
        }
        
        public SecondServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SecondServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SecondServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SecondServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public WPFConsumer.DemoService.GetUsersResponse GetUsers(WPFConsumer.DemoService.GetUsersRequest request) {
            return base.Channel.GetUsers(request);
        }
        
        public System.Threading.Tasks.Task<WPFConsumer.DemoService.GetUsersResponse> GetUsersAsync(WPFConsumer.DemoService.GetUsersRequest request) {
            return base.Channel.GetUsersAsync(request);
        }
        
        public WPFConsumer.DemoService.GetEventResponse GetEvent(WPFConsumer.DemoService.GetEventRequest request) {
            return base.Channel.GetEvent(request);
        }
        
        public System.Threading.Tasks.Task<WPFConsumer.DemoService.GetEventResponse> GetEventAsync(WPFConsumer.DemoService.GetEventRequest request) {
            return base.Channel.GetEventAsync(request);
        }
        
        public WPFConsumer.DemoService.GetEventsByLocationResponse GetEventsByLocation(WPFConsumer.DemoService.GetEventsByLocationRequest request) {
            return base.Channel.GetEventsByLocation(request);
        }
        
        public System.Threading.Tasks.Task<WPFConsumer.DemoService.GetEventsByLocationResponse> GetEventsByLocationAsync(WPFConsumer.DemoService.GetEventsByLocationRequest request) {
            return base.Channel.GetEventsByLocationAsync(request);
        }
        
        public WPFConsumer.DemoService.GetLocationsResponse GetLocations(WPFConsumer.DemoService.GetLocationsRequest request) {
            return base.Channel.GetLocations(request);
        }
        
        public System.Threading.Tasks.Task<WPFConsumer.DemoService.GetLocationsResponse> GetLocationsAsync(WPFConsumer.DemoService.GetLocationsRequest request) {
            return base.Channel.GetLocationsAsync(request);
        }
        
        public WPFConsumer.DemoService.GetUserResponse GetUser(WPFConsumer.DemoService.GetUserRequest request) {
            return base.Channel.GetUser(request);
        }
        
        public System.Threading.Tasks.Task<WPFConsumer.DemoService.GetUserResponse> GetUserAsync(WPFConsumer.DemoService.GetUserRequest request) {
            return base.Channel.GetUserAsync(request);
        }
    }
}
