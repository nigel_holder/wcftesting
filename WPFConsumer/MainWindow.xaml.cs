﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.Serialization;

namespace WPFConsumer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            PopulateLocations();

        }

        private void PopulateLocations()
        {
            try
            {
                var client = new DemoService.SecondServiceClient();
                var locations = client.GetLocations(new DemoService.GetLocationsRequest());
                LocationsCombo.ItemsSource = locations.GetLocationsResult.ToList();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " + ex.InnerException);
            }
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var demo = new DemoService.SecondServiceClient();
            var events = demo.GetEventsByLocation(new DemoService.GetEventsByLocationRequest((int)LocationsCombo.SelectedValue));




        }

        private void PeopleButton_Click(object sender, RoutedEventArgs e)
        {
            var demo = new DemoService.SecondServiceClient();
            var data = demo.GetUsers(new DemoService.GetUsersRequest());
            Grid.ItemsSource = data.GetUsersResult.Select(x => new { Name = x.Name, DateOfBirth = x.DateOfBirth, Age = (DateTime.Today.Subtract(x.DateOfBirth).Days) / 365 }).ToList();

        }
    }
}
